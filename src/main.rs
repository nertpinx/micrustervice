/*
#![feature(proc_macro_hygiene, decl_macro)]

use rocket::{self, get, routes};

#[get("/")]
fn hello() -> String {
    let mut ret = r"<?xml version='1.0' encoding='UTF-8'?>".to_string();
    ret.push_str(&Response::new().say("Hello World").build().unwrap());
    ret
}

fn main() {
    rocket::ignite().mount("/", routes![hello]).launch();
}
*/

use failure::{Error, ResultExt};
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Method, Request, Response, Server, StatusCode};
use log::{error, info};

use std::convert::Into;

async fn hello_world(request: Request<Body>) -> Result<Response<Body>, http::Error> {
    info!("Mucrustervice received a request: {:#?}", request);

    let builder = Response::builder();

    match (request.method(), request.uri().path()) {
        (&Method::GET, "/") => builder.body(Body::from("Try POSTing data to /echo")),
        (&Method::POST, "/echo") => builder.body(request.into_body()),
        _ => builder.status(StatusCode::NOT_FOUND).body(Body::empty()),
    }
}

async fn signal_shutdown() {
    tokio::signal::ctrl_c()
        .await
        .expect("Could not install signal handler");
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    pretty_env_logger::init();

    let localhost6 = [0; 8];

    let addr = (localhost6, 8000).into();

    let make_svc = make_service_fn(|_conn| async { Ok::<_, http::Error>(service_fn(hello_world)) });

    info!("Starting");

    if let Ok(asdf) = std::env::var("STOP_NOW") {
        error!("{}", asdf);
        eprintln!("eprintln");
        println!("println");
        std::process::exit(1);
    }


    Server::bind(&addr)
        .serve(make_svc)
        .with_graceful_shutdown(signal_shutdown())
        .await
        .context("Cannot start server")?;

    Ok(())
}
