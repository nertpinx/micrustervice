use failure::{Error, ResultExt};
use hyper::{Request, Client, Method, Body};

#[tokio::main]
async fn main() -> Result<(), Error> {
    let client = Client::new();

    let request = Request::builder()
        .method(Method::POST)
        .uri("http://localhost:8000/echo")
        .header("content-type", "application/json")
        .body(Body::from(r#"{"asdf": "fdsa"}"#))
        .context("I don't GET it ;-)")?;

    let response = client.request(request).await?;

    println!("Response: {:#?}", response);

    Ok(())
}
