FROM scratch

COPY target/x86_64-unknown-linux-musl/release/micrustervice /micrustervice

STOPSIGNAL SIGINT

ENTRYPOINT ["/micrustervice"]
